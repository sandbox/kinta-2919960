### About

Get media provided by a telegram bot in a group

With this module one can get media from a telegram own bot to any drupal site. Once installed and bot is created through botfather bot:

### Creating bot in telegram

1.  Start a chat with botfather and create a bot writing:

    `/newbot`

2.  `BOTFATHER> Alright, a new bot. How are we going to call it? Please choose a name for your bot.  
    botname`

3.  `>BOTFATHER> Good. Now let's choose a username for your bot. It must end in `bot`. Like this, for example: TetrisBot or tetris_bot.  
    BotName`

4.  In this response copy the access token.

    `BOTFATHER> Done! Congratulations on your new bot. You will find it at t.me/BotName. You can now add a description, about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully operational before you do this.  
    BOTFATHER> Use this token to access the HTTP API:  
    BOTFATHER> 111111:1dkceowvroopqfo41ocra BOTFATHER> For a description of the Bot API, see this page: https://core.telegram.org/bots/api`

5.  Set privacy to not saturate your server with the processing of all messages.  
    `/setprivacy  
    BOTFATHER> 'Enable' - your bot will only receive messages that either start with the '/' symbol or mention the bot by username.  
    BOTFATHER> 'Disable' - your bot will receive all messages that people send to groups.` Choose Enable
6.  Set commands to write a quick bot command call through [/] in message input box.  
    `/setcommands  
    BOTFATHER> OK. Send me a list of commands for your bot. Please use this format:  
    BOTFATHER> command1 - Description  
    BOTFATHER> command2 - Another description  
    command1 -Description  
    `
7.  The telegram part is done, you can talk to your bot or add it to any group.

### Setting the module

In Drupal part first of all setting the hook must be done, in [hooks settings](/ca/admin/config/services/telegram_media/hooks) it can be set to get user synced between telegram and site, a text field must be added to user entity. This field will be used to set the user id of saved media. If field doesn't exist a fallback user will be choosen, and if no fallback user is defined the user with uid 1 will be used. In [field mappings](/ca/admin/config/services/telegram_media/hooks/field_mapping) the telegram username field and fallback user can be set. In the same [field mappings](/ca/admin/config/services/telegram_media/hooks/field_mapping) settings the relation between obtained media and drupal content type can be set.

### Uses

To use this media collection tool from telegram(groups or directly) to any Drupal site, from a chat where bot is present launch:

`/c absolute_url description #tag #tag2` where description and tags are optionals.  
Also it can be used replying to any link sent, with just:  
`/c description #tag #tag1`  
Again, description and tags are optional.

A new content will be created in site using the content type and the fields as specified

