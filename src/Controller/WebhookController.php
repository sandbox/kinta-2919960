<?php

namespace Drupal\telegram_media\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManager;


/**
 * Class WebhookController.
 */
class WebhookController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;
  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new WebhookController object.
   */
  public function __construct(ConfigFactory $config_factory, EntityTypeManager $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Token.
   *
   * @return string
   *   Return Hello string.
   */
  public function token($token) {
    $hook_settings = $this->configFactory->get('telegram_media.hooks_settings');
    
    if ($token == $hook_settings->get('url_token')){
      $authorized = "TRUE";
    }else{
       // @todo forbidden html code
       return [
        '#type' => 'markup',
        '#markup' => $this->t("access denied"),
      ];
    }

//comment to debug:
try {
    // Create Telegram API object
    $telegram = new \Longman\TelegramBot\Telegram($hook_settings->get("bot_api_key"), $hook_settings->get("bot_username"));
    //$telegram = new \Drupal\telegram_media\TelegramMedia($hook_settings->get("bot_api_key"), $hook_settings->get("bot_username"));

    $telegram->addCommandsPaths([
      __DIR__.'/../Commands',
    ]);
    // Handle telegram webhook request
//error_log("hi");
$telegram->handle();
//ddl($telegram->getMessage());
//\Drupal::logger('my_module')->notice(print_r($telegram->getMessage(), TRUE));
} catch (\Longman\TelegramBot\Exception\TelegramException $e) {
    // Silence is golden!
    // log telegram errors
    error_log("bad bad");
     echo $e->getMessage();
} catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
  error_log("bad bad");
}
    return [
      '#type' => 'markup',
      '#markup' => $this->t("Implement method: token with parameter(s):"),
    ];
  }

}
