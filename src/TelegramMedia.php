<?php
/**
 * @file
 * This file is to extend the Telegram class to be able to process all incoming
 * messages. By now is not used.
 */ 
namespace Drupal\telegram_media;

class TelegramMedia extends \Longman\TelegramBot\Telegram {
  protected $message ;
  public function processUpdate(\Longman\TelegramBot\Entities\Update $update) {
    if($message = $update->getMessage()) {
      $this->message = $message;
      $sender = $message->getFrom();
      $sender_id = $sender->getId();
      $sender_username = $sender->getUsername();
      $chat = $message->getChat();
      $chat_id = $chat->getId();
      $message_text = $message->getText();
    }
    return parent::processUpdate($update);
  }
  public function getMessage(){
    return $this->message;
  }
}
