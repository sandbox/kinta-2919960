<?php

namespace Drupal\telegram_media\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HooksSettingsForm.
 */
class HooksSettingsForm extends ConfigFormBase {
  /**
   * @var EntityTypeManagerInterface $entityManager
   */
  protected $entityManager;

  /**
   * @var EntityFieldManagerInterface $fieldManager
   */
  protected $fieldManager;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, entityTypeManagerInterface $entity_manager, EntityFieldManagerInterface $field_manager) {
    parent::__construct($config_factory);
    $this->entityManager = $entity_manager;
		$this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hooks_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'telegram_media.hooks_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $media_types = [
        'capture_links',
        'capture_images',
        'capture videos',
        'capture music',
        'capture files',
        'capture contacts',
        'capture locations',
        'capture galleries',
        'capture audio',
    ];

    $config = $this->config('telegram_media.hooks_settings');
    $form['bot_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bot API key'),
      '#description' => $this->t('The key provided by botfather'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('bot_api_key'),
    ];
    $form['bot_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bot username'),
      '#description' => $this->t('The bot name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('bot_username'),
    ];
    $form['url_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url token'),
      '#description' => $this->t('The url token'),
      '#default_value' => $config->get('url_token')?:Crypt::randomBytesBase64(),
    ];
    $form['hooks_to_set'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Hooks to set'),
      '#description' => $this->t('The media messages that you want this web to capture'),
      '#options' => [
        'capture_links' => $this->t('capture links'),
        /*'capture_images' => $this->t('capture images'),
        'capture videos' => $this->t('capture videos'),
        'capture music' => $this->t('capture music'),
        'capture files' => $this->t('capture files'),
        'capture contacts' => $this->t('capture contacts'),
        'capture locations' => $this->t('capture locations'),
        'capture galleries' => $this->t('capture galleries'),
        'capture audio' => $this->t('capture audio')*/
      ],
      '#default_value' => [
        $config->get('capture_links'),
        /*$config->get('capture_images'),
        $config->get('capture_videos'),
        $config->get('capture_music'),
        $config->get('capture_files'),
        $config->get('capture_contacts'),
        $config->get('capture_locations'),
        $config->get('capture_galleries'),
        $config->get('capture_audio'),*/
      ]
    ];
    $form['actions']['delete hook'] = [
      '#type' => 'submit',
      '#value' => $this->t('delete webhook'),
      '#submit' => array('::deleteWebhook'),
      '#disabled' => null == $config->get('bot_api_key'),//$config->get('authentication.gnusocial_token') == "" && $config->get('authentication.gnusocial_token_secret') == "",
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#disabled'] = null !== $config->get('bot_api_key');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteWebhook(array &$form, FormStateInterface $form_state) {
		try {
				// Create Telegram API object
				$telegram = new \Longman\TelegramBot\Telegram($form_state->getValue('bot_api_key'), $form_state->getValue('bot_username'));
				// Delete webhook
				$result = $telegram->deleteWebhook();
				if ($result->isOk()) {
						drupal_set_message($result->getDescription());
						// @todo remove all stored settings 
						$this->config('telegram_media.hooks_settings')->delete();
				}
		} catch (Longman\TelegramBot\Exception\TelegramException $e) {
				drupal_set_message($e->getMessage());
		} 
    parent::validateForm($form, $form_state);
  }



  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      // Create Telegram API object
      $telegram = new \Longman\TelegramBot\Telegram($form_state->getValue('bot_api_key'), $form_state->getValue('bot_username'));

      // Set webhook
      $result = $telegram->setWebhook('https://planet.communia.org/telegram_media/' . $form_state->getValue('url_token'));
      if ($result->isOk()) {
        drupal_set_message($result->getDescription());
				// Now that result is ok, store this configuration to allow removal, update 
				// later.

        // Retrieve the configuration
        \Drupal::configFactory()->getEditable('telegram_media.hooks_settings')
        // Set the submitted configuration setting
          ->set('bot_api_key', $form_state->getValue('bot_api_key'))
          ->set('bot_username', $form_state->getValue('bot_username'))
          ->set('url_token', $form_state->getValue('url_token'))
          ->set('capture_links', $form_state->getValue('hooks_to_set')['capture_links'])
          ->set('capture_images', $form_state->getValue('hooks_to_set')['capture_images'])
          ->set('capture_videos', $form_state->getValue('hooks_to_set')['capture_videos'])
          ->set('capture_music', $form_state->getValue('hooks_to_set')['capture_music'])
          ->set('capture_files', $form_state->getValue('hooks_to_set')['capture_files'])
          ->set('capture_contacts', $form_state->getValue('hooks_to_set')['capture_contacts'])
          ->set('capture_locations', $form_state->getValue('hooks_to_set')['capture_locations'])
          ->set('capture_galleries', $form_state->getValue('hooks_to_set')['capture_galleries'])
          ->set('capture_audio', $form_state->getValue('hooks_to_set')['capture_audio'])
          ->save();
      }
    } catch (Longman\TelegramBot\Exception\TelegramException $e) {
      // log telegram errors
      drupal_set_message($e->getMessage());
    }
  }

}
