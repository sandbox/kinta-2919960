<?php

namespace Drupal\telegram_media\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityFieldManager;
use \Drupal\Core\Url;

/**
 * Class FieldMappingForm.
 */
class FieldMappingForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  /**
   * Drupal\Core\Entity\EntityFieldManager definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;


  /**
   * Constructs a new TelegramMediaFieldMappingForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManager $entity_type_manager,
    EntityFieldManager $entity_field_manager
    ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'telegram_media.field_mapping',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telegram_media_field_mapping_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('telegram_media.field_mapping');
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    $form['#tree'] = TRUE;

    $form['tg_user'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Telegram user lookup'),
        '#prefix' => '<div id="tg_user-wrapper">',
        '#suffix' => '</div>',
      ];

    $fields = $this->entityFieldManager->getFieldDefinitions('user','user');

    $opts = $this->fieldsOptions('username', $fields, ['string','text','link']);
    $user_field_ui = Url::fromRoute('entity.user.field_ui_fields')->toString();

    $form['tg_user']['username_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Telegram username field'),
      '#description' => $this->t('The <a href="@url">field in user</a> that holds telegram username', ["@url" => $user_field_ui]),
      '#options' => $opts,
      '#empty_value' => '',
      '#default_value' => $config->get('tg_user.username_field'),
    ];
    $form['tg_user']['bot_user'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Bot user'),
      '#target_type' => 'user',
      '#description' => $this->t('The user that will act as author of published media, when no telegram username field is found in this site'),
      '#default_value' => entity_load('user',$config->get('tg_user.bot_user')),
    ];



    foreach ($this->getMediaTypes() as $media_type => $media_type_label){
      // We need this to fill the options according the selected item, 
      // both stored in config or from form_state when using ajax
      $selected_content_type = ($form_state->getValue([$media_type,'content_type']) !== null)?$form_state->getValue([$media_type,'content_type']):$config->get($media_type.'.content_type');
      // All the fields of the selected type.
      $fields = $this->entityFieldManager->getFieldDefinitions('node',$selected_content_type);

      $form[$media_type] = [
        '#type' => 'fieldset',
        '#title' => $this->t('%label mapping',[ '%label' => $media_type_label]),
        '#prefix' => '<div id="' . $media_type . '-fieldset-wrapper">',
        '#suffix' => '</div>',
      ];
      $form[$media_type]['content_type'] = [
        '#type' => 'select',
        '#title' => $this->t( $media_type_label . ' content type'),
        '#description' => $this->t('The content that will be used to ' . $media_type_label),
        '#options' => $contentTypesList,
        '#empty_value' => '',
        '#default_value' => $config->get($media_type.'.content_type'),
        '#ajax' => [
          'callback' => '::checkFieldsCallback',
          'wrapper' => $media_type . '-fields-wrapper',
          'method' => 'replace',
        ],
      ];
      $form[$media_type]['fields'] = [
				'#type' => 'container',
        '#attributes' => ['id' => $media_type . '-fields-wrapper']
      ];

      $opts = $this->fieldsOptions('vocabulary', $fields, ['entity_reference']);

      $form[$media_type]['fields']['vocabulary'] = [
        '#type' => 'select',
        '#title' => $this->t($media_type_label . ' vocabulary'),
        '#description' => $this->t('Vocabulary of ' . $media_type_label),
        '#options' => $opts ,
        '#empty_value' => '',
        '#default_value' => $config->get($media_type . '.fields.vocabulary'),
        '#states' => [
          'visible' => [
            ':input[name="capture_links[content_type]"]' => [ '!value' => "" ],
          ]
        ]
      ];

      $opts = $this->fieldsOptions('description', $fields, ['text_with_summary', 'string']);

      $form[$media_type]['fields']['description'] = [
        '#type' => 'select',
        '#title' => $this->t($media_type_label . ' description'),
        '#description' => $this->t('The description of the ' . $media_type_label),
        '#options' => $opts,
        '#empty_value' => '',
        '#default_value' => $config->get($media_type . '.fields.description'),
        '#states' => [
          'visible' => [
            ':input[name="capture_links[content_type]"]' => [ '!value' => "" ],
          ]
        ]
      ];
      switch ($media_type){
        case 'capture_links':
          // the filter for image will be file_field
          $opts = $this->fieldsOptions('link', $fields, ['string', 'link','text_with_summary']);
          $form[$media_type]['fields']['link'] = [
            '#type' => 'select',
            '#title' => $this->t('link'),
            '#description' => $this->t('The field that will be used as link'),
            '#options' => $opts,
            '#empty_value' => '',
            '#default_value' => $config->get($media_type . '.fields.link'),
            '#states' => [
              'visible' => [
                ':input[name="capture_links[content_type]"]' => [ '!value' => "" ], 
              ]
            ]
          ];
          break;
      }
    }
    return parent::buildForm($form, $form_state);
  }
  /**
   * Get capturable Media Types.
   *
   * @return array
   *   List of media types
   */
  protected function getMediaTypes(){
    // @todo Obtain enabled media_types from hook config and filter
    // By now only capture links is implemented, @todo

    /**
     * List of captured media types.
     * @var array 
     */
     $enabled_services = [
          'capture_links' => $this->t('capture links'),
          'capture_images' => $this->t('capture images'),
          'capture_videos' => $this->t('capture videos'),
          'capture_music' => $this->t('capture music'),
          'capture_files' => $this->t('capture files'),
          'capture_contacts' => $this->t('capture contacts'),
          'capture_locations' => $this->t('capture locations'),
          'capture_galleries' => $this->t('capture galleries'),
          'capture_audio' => $this->t('capture audio')
        ];
       $enabled_mappings = [];
       foreach ($enabled_services as $key => $label){
         if ($this->config('telegram_media.hooks_settings')->get($key)){
           $enabled_mappings[$key] = $label;
         }
       }
     return $enabled_mappings;
  }
  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function checkFieldsCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $capture_type = $element['#parents'][0];
    $form_state->setRebuild('true');
    return $form[$capture_type]['fields'];
  }

  /**
   * Creates a list of options from fieldDefinitions list
   *
   * @var string $field_name
   *   The field mapping key
   * @var \Drupal\Core\Field\FieldDefinitionInterface[] $fieldDefs
   *   The list of fields converted to selectable options array
   * @var string[] $type
   *   Optional array with datatype list filters.
   *
   * @return array
   *   List of the fields as select element options
   */
  protected function fieldsOptions($field_name, array $fieldDefs, array $type=[]){
    $options = [];
    foreach ($fieldDefs as $definition){
      if(in_array( $definition->getType(), $type ) || empty($type)){
        if ($field_name != "vocabulary" || $definition->getSetting('target_type') == 'taxonomy_term'){
          $options[$definition->getName()] = $definition->getLabel();
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('telegram_media.field_mapping');
    $config->delete();
    $config->set('tg_user.username_field', $form_state->getValue(['tg_user','username_field']));
    $config->set('tg_user.bot_user', $form_state->getValue(['tg_user','bot_user']));
    foreach ($this->getMediaTypes() as $media_type => $media_label){
      if($form_state->getValue([$media_type,'content_type']) != ""){
        $config->set($media_type . '.content_type', $form_state->getValue([$media_type,'content_type']));
        foreach($form_state->getValues()[$media_type]['fields'] as $field => $value){
          $config->set($media_type . '.fields.' . $field, $form_state->getValue([$media_type,'fields',$field]));
        }
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
