<?php

namespace Longman\TelegramBot\Commands\UserCommands;


use \Longman\TelegramBot\Commands\UserCommand;
use \Longman\TelegramBot\Request;
use \Longman\TelegramBot\Telegram;
use \Longman\TelegramBot\Entities\Update;
use \Longman\TelegramBot\Entities\ServerResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \Drupal\node\NodeInterface;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\Component\Utility\Unicode;

class CCommand extends UserCommand{
  protected $name = 'c';                      // Your command's name
  // Your command description
  protected $description = 'A command to collect media in external site';
  // Usage of your command
  protected $usage = "/c or /c <media> [description] [#hashtag]\nOr replying a link with:\n/c [description] [#hashtag]";
  protected $version = '1.0.0';                  // Version of your command

    /**
     * Constructor
     *
     * @param \Longman\TelegramBot\Telegram        $telegram
     * @param \Longman\TelegramBot\Entities\Update $update
     */
    public function __construct(Telegram $telegram, Update $update = null)
    {
        $this->telegram = $telegram;
        $this->setUpdate($update);
        $this->config = $telegram->getCommandConfig($this->name);
        $this->entityTypeManager = \Drupal::service('entity_type.manager');
        $this->configFactory = \Drupal::service('config.factory');
        $this->entityQuery = \Drupal::service('entity.query');
    }

  public function execute(): ServerResponse
  {
    $config_hook = $this->configFactory->get('telegram_media.hooks_settings');
    $config_mapping = $this->configFactory->get('telegram_media.field_mapping');

    $message = $this->getMessage();            // Get Message object

		$chat_id = $message->getChat()->getId();   // Get the current Chat ID

		$data = [                                  // Set up the new message data
				'chat_id' => $chat_id,                 // Set Chat ID to send the message to
    ];
    $data['text'] = t("No media detected.\nUsage:\n". $this->usage . "\n(Note that by now only urls are processed)");

    $out = [];

    $remain_text = $message->getText();
    $links = [];
    $hashtags = [];
    //$images = []; // @todo implement other entities;


    // Check for media in replied root message
    if ($message->getReplyToMessage()){
      foreach($message->getReplyToMessage()->getEntities() as $entity){
        $value = mb_substr($message->getReplyToMessage()->getText(), $entity->getOffset(), $entity->getLength());
        switch($entity->getType()){
          case "url":
            $links[] = $value;
            $replied_author = $message->getFrom()->getUserName();
            break;
          case "image":
            // @todo implement other entities;
            break;
        }
      }
    }
    // Check for media in current message
    foreach ( $message->getEntities() as $entity){
      $value = mb_substr($message->getText(), $entity->getOffset(), $entity->getLength());
      switch($entity->getType()){
        case 'url':
          $links[] = $value; 
          break;
        case 'image':
          // @todo implement other entities;
          break;
        case 'hashtag':
          // Remove leading #
          $value[0] = "";

          //$hashtags[] = mb_ereg_replace('^[[:space:]]*([\s\S]*\S)?[[:space:]]*$', '\1', $value );
          $hashtags[] = trim($value);
          // put back # to perform str_replace below
          $value = "#" . trim($value);
          break;
      }
      // Clear values in the text description.
      $remain_text = trim(mb_eregi_replace("(^| )".preg_quote($value, "/")."($| )", " ", $remain_text));
    }

    $author = $replied_author?:$message->getFrom()->getUsername();
    // Link media case
    if ($config_hook->get('capture_links') && count($links) > 0){
      $node = $this->entityTypeManager->getStorage('node')->create([
        'type' => $config_mapping->get('capture_links.content_type'),
        'uid' => $this->getUserFromTelegramUserName($author),
        $config_mapping->get('capture_links.fields.link') => $links,
        $config_mapping->get('capture_links.fields.description') => trim($remain_text),
        'title' => Unicode::truncate(trim($remain_text)?:$links[0], 255, TRUE, TRUE),
      ]);
      if($config_mapping->get('capture_links.fields.vocabulary')){
        $vocabulary_field = $config_mapping->get('capture_links.fields.vocabulary');
        $tids = $this->getOrCreate($node, $vocabulary_field, $hashtags);
        $node->set($config_mapping->get('capture_links.fields.vocabulary'), $tids);
      }
      $node->save();
      $site_link = $node->toUrl('canonical', ["absolute" => TRUE])->toString();
      $data['text'] = t("Link collected in @url", ["@url" => $site_link]);
    }

    /*// Other media cases @todo not implemented
    if ($config_hook->get('capture_images')){
    }
    */

    //DBG
    //$out['user'] = $message->getFrom()->getUsername();
    //$out['text'] = trim($remain_text);
    //$out['links'] = $links;
    //$out['hashtags'] = $hashtags;
    //$out['author'] = $author;
    //$out['msg'] = print_r($message,TRUE);
    //$out['saved'] = $node->id();


    //$data['text'] = print_r($out, TRUE); //DBG
		return Request::sendMessage($data);        // Send message!
  }

  /**
   * Get or create all the taxonomies indicated
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node which vocabulary terms will be stored
   * @param string $vocabulary_field
   *   The name of the field which stores vocabulary terms
   * @param array $terms
   *   An array with terms to be used or created
   *
   * @return array
   *   The ids of the terms used.
   */
  protected function getOrCreate(NodeInterface $node, $vocabulary_field, array $terms){
    //get First bundle:
    $target_vocabulary = array_keys($node->getFieldDefinition($vocabulary_field)->getSetting("handler_settings")['target_bundles'])[0];
    // if failing check entity_autocomplete check of target bundles in drupal
    // api.
    $ctids = [];
    foreach($terms as $term){
      $target_terms = taxonomy_term_load_multiple_by_name($term, $target_vocabulary);
      $target_term = current($target_terms);
      $vid = \Drupal::entityManager()->getStorage('taxonomy_term')->loadTree($target_vocabulary);
      if($target_term == NULL) {
         $target_term = Term::create([
           'name' => $term,
           'vid' => taxonomy_vocabulary_get_names()[$target_vocabulary]
         ]);
         $target_term->save();
      }
      $ctids[] = $target_term->id();
    }
    return $ctids;
  }

  /**
   * Get user name from specified telegram alias field, or the defined user if
   * not found.
   *
   * @param string $tg_username
   *   The telegram username
   *
   * @return User
   *   The drupal user id of user found with the username defined in specified
   *   field, the fallback specified bot user or finally the user with uid 1.
   */
  protected function getUserFromTelegramUserName($tg_username){
    $config_mapping = $this->configFactory->get('telegram_media.field_mapping');
    if ($config_mapping->get('tg_user.username_field')){
      $query = $this->entityQuery
      ->get('user')
      ->condition($config_mapping->get('tg_user.username_field'), $tg_username);
      $user_ids = $query->execute();
      if ($user_ids){
        return array_keys($user_ids)[0];
      }
    }
    return $config_mapping->get('tg_user.bot_user')?:1;
  }
}
